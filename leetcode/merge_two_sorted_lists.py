# You are given the heads of two sorted linked lists list1 and list2.

# Merge the two lists in a one sorted list. The list should be made
# by splicing together the nodes of the first two lists.

# Return the head of the merged linked list.


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


def merge_two_sorted_lists(l1, l2):
    dummy = temp = ListNode()

    while l1 and l2:
        if l1.val < l2.val:
            temp.next = l1
            l1 = l1.next 
        else:
            temp.next = l2
            l2 = l2.next 

        temp = temp.next 

    temp.next = l1 or l2
    return dummy.next 