# Given an array of strings strs, group the anagrams together. 
# You can return the answer in any order.

# An Anagram is a word or phrase formed by rearranging the letters 
# of a different word 
# or phrase, typically using all the original letters exactly once.

def group_anagrams(strs):
    words = {}
    result = []

    for word in strs:
        sorted_word = ''.join(sorted(word))
        if sorted_word not in words:
            words[sorted_word] = [word]
        else:
            words[sorted_word].append(word)

    for group in words.values():
        result.append(group)
    return result

print(group_anagrams(["eat","tea","tan","ate","nat","bat"]))