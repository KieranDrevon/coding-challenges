# Write a function that checks if a given string (case insensitive) is a palindrome.


def is_palindrome(s):
    lower = s.lower()
    l = 0
    r = len(lower) - 1
    
    while l <= r:
        if lower[l] == lower[r]:
            l += 1
            r -= 1
        else:
            return False
    return True