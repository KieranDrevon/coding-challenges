# Complete the function that accepts a string parameter, and reverses each word in the string. All spaces in the string should be retained.

# Examples
# "This is an example!" ==> "sihT si na !elpmaxe"
# "double  spaces"      ==> "elbuod  secaps"

def reverse_words(text):
#     return "".join(reversed(text))

#     list_of_words = text.split()
#     result_list = []
#     container_string = ''
    
#     for word in list_of_words:
#         for char in word[::-1]:
#             container_string += char
#         result_list.append(container_string)
#         container_string = ''
        
#     return ' '.join(result_list)

    word_list = []
    for word in text.split(' '):
          word_list.append(word[::-1])
    return ' '.join(word_list)