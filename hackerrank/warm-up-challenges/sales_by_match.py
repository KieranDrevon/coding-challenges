# There is a large pile of socks that must be paired by color.
# Given an array of integers representing the color of each sock,
# determine how many pairs of socks with matching colors there are.

def sockMerchant(n, ar):
    # Write your code here
    d = {}
    pairs = 0
    for c in ar:
        if c not in d:
            d[c] = 1
        else:
            d[c] += 1
    
    for value in d.values():
        pairs += value // 2
    
    return pairs