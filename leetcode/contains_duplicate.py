# Given an integer array nums, return true if any value 
# appears at least twice in the array, and return false if 
# every element is distinct.

def contains_duplicate(nums):
    d = {}
    for num in nums:
        if num in d:
            return True
        d[num] = 1 + d.get(num,0)
    return False