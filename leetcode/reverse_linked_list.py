# Given the head of a singly linked list, reverse the list, 
# and return the reversed list.

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def reverseList(self, head: ListNode) -> ListNode:
        curr = head
        prev = None

        while curr:
            nxt = curr.next
            curr.next = prev
            prev = curr
            curr = nxt
        return prev


# Working on understanding the recursive solution but really burnt out and struggling. Will try again tomorrow, no more energy for this stuff today

# Recursive solution completed, still iffy, but in a much better place. 

def reverseList(self, head: ListNode):
    if not head:
        return None

    newHead = head
    if head.next:
        newHead = self.reverseList(head.next)
        head.next.next = head
    head.next = None
    return newHead