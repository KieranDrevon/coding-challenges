# Write a function to convert a name into initials. This kata strictly takes two words with one space in between them.

# The output should be two capital letters with a dot separating them.

# It should look like this:

# Sam Harris => S.H

# patrick feeney => P.F

def abbrev_name(name):
    # create result string
    # isolate both names
    # take the first index of both names, capitalize them and add to result w/ a dot
    
    names = name.split(' ')
    
    first_name = names[0]
    last_name = names[1]
    
    return f"{first_name[0].upper()}.{last_name[0].upper()}"