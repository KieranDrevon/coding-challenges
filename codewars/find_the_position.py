# When provided with a letter, return its position in the alphabet.

# Input :: "a"

# Ouput :: "Position of alphabet: 1"

import string 

def position(alphabet):
    letters = list(string.ascii_lowercase)
    
    return f"Position of alphabet: {letters.index(alphabet) + 1}"