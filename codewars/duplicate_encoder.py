# The goal of this exercise is to convert a string to a new string where each character in the new string is "(" if that character appears only once in the original string, or ")" if that character appears more than once in the original string. Ignore capitalization when determining if a character is a duplicate.

# Examples
# "din"      =>  "((("
# "recede"   =>  "()()()"
# "Success"  =>  ")())())"
# "(( @"     =>  "))((" 
# Notes
# Assertion messages may be unclear about what they display in some languages. If you read "...It Should encode XXX", the "XXX" is the expected result, not the input!

def duplicate_encode(word):
    # create result string 
    # create hash map of word, key: char value: freq
    # loop through hashmap, if char freq > 1 then add )
    # else add ( 
    
    res = ''
    map = {}
    
    for char in word.lower():
        if char not in map:
            map[char] = 1
        else:
            map[char] += 1
            
    print(map)
            
    for char in word.lower():
        if map[char] > 1:
            res += ')'
        else:
            res += '('
    return res