# Scenario
# Several people are standing in a row divided into two teams.
# The first person goes into team 1, the second goes into team 2, the third goes into team 1, and so on.

# Task
# Given an array of positive integers (the weights of the people), return a new array/tuple of two integers, where the first one is the total weight of team 1, and the second one is the total weight of team 2.

def row_weights(array):
    team_one = 0
    team_two = 0
    
    for i, weight in enumerate(array):
        if i % 2 == 0:
            team_one += weight
        else:
            team_two += weight
            
    return (team_one, team_two)