# If anything in the text isn't a letter, ignore it and don't return it.

# "a" = 1, "b" = 2, etc.

# Example
# alphabet_position("The sunset sets at twelve o' clock.")
# Should return "20 8 5 19 21 14 19 5 20 19 5 20 19 1 20 20 23 5 12 22 5 15 3 12 15 3 11" ( as a string )

import string

def alphabet_position(text):
    alphabet_dict = {letter: index for index, letter in enumerate(string.ascii_lowercase, start=1)}
    res = []
    
    for char in text.lower():
        if char.isalpha():
            res.append(str(alphabet_dict[char]))

    return ' '.join(res)