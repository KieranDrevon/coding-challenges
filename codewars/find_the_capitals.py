# Write a function that takes a single string (word) as argument. The function must return an ordered list containing the indexes of all capital letters in the string.
# Late and fast submission, busy day today!

def capitals(word):
    result = []
    for index, char in enumerate(word):
        if char.isupper():
            result.append(index)
    return result