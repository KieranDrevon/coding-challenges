# Character recognition software is widely used to digitise printed texts. Thus the texts can be edited, searched and stored on a computer.

# When documents (especially pretty old ones written with a typewriter), are digitised character recognition softwares often make mistakes.

# Your task is correct the errors in the digitised text. You only have to handle the following mistakes:

# S is misinterpreted as 5
# O is misinterpreted as 0
# I is misinterpreted as 1

def correct(s):
    map = {"5": "S", "0": 'O', "1": "I"}
    res = ''
    
    for char in s:
        if char in map:
            res += map[char]
            continue
        res += char
    return res