# Description:
# Replace all vowel to exclamation mark in the sentence. aeiouAEIOU is vowel.

# Examples
# replace("Hi!") === "H!!"
# replace("!Hi! Hi!") === "!H!! H!!"
# replace("aeiou") === "!!!!!"
# replace("ABCDE") === "!BCD!"

# Long day, brain is fried. Someone hire me as an engineer already please....

def replace_exclamation(s):
    
    vowels = 'aAeEiIoOuU'
    res = ''
    
    for i in range(len(s)):
        if s[i] in vowels:
            res += '!'
        else:
            res += s[i]
    return res