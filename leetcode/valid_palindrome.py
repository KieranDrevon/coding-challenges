# A phrase is a palindrome if, after converting all uppercase letters 
# into lowercase letters and removing all non-alphanumeric characters, 
# it reads the same forward and backward. Alphanumeric characters 
# include letters and numbers.

# Given a string s, return true if it is a palindrome,
# or false otherwise.



def valid_palindrome(s):
    lowercase_string = ''

    for char in s:
        if char.isalnum():
            lowercase_string += char.lower()

    l = 0
    r = len(lowercase_string) - 1

    while l <= r:
        if lowercase_string[l] == lowercase_string[r]:
            l += 1
            r -= 1
        else:
            return False
    return True

print(valid_palindrome('racecar'))