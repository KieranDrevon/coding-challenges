# Given a non-empty array of integers, return the 
# result of multiplying the values
# together in order.

def grow(arr):
    product = 1
    for num in arr:
        product *= num
    return product