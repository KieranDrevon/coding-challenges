# Given an array of integers, find the one that appears an odd number of times.

# There will always be only one integer that appears an odd number of times.

# Examples
# [7] should return 7, because it occurs 1 time (which is odd).
# [0] should return 0, because it occurs 1 time (which is odd).
# [1,1,2] should return 2, because it occurs 1 time (which is odd).
# [0,1,0,1,0] should return 0, because it occurs 3 times (which is odd).
# [1,2,2,3,3,3,4,3,3,3,2,2,1] should return 4, because it appears 1 time (which is odd).

def find_it(seq):
    # create hashmap of the seq
    # key will be string of the value, val will be the frequency
    # loop through the map and check all values, identify if val is odd and return the key in int form
    
    map = {}
    
    for num in seq:
        if str(num) not in map:
            map[str(num)] = 1
        else:
            map[str(num)] += 1
            
            
    for k, v in map.items():
        if v % 2 == 0:
            continue
        else:
            return int(k)