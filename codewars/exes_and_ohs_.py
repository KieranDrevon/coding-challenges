# Check to see if a string has the same amount of 'x's and
# 'o's. The method must return a boolean and be case 
# insensitive. The string can contain any char.


def xo(s):
    lower_string = s.lower()
    x_count = 0
    o_count = 0
    
    if "x" not in lower_string and "o" not in lower_string:
        return True
    
    for c in lower_string:
        if c == 'x':
            x_count += 1
        elif c == 'o':
            o_count += 1
            
    if x_count == o_count:
        return True
    else:
        return False