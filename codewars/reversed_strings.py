# Complete the solution so that it reverses the string passed into it.

# 'world'  =>  'dlrow'
# 'word'   =>  'drow'

# My Hawaii vacation is coming up, We leave on monday! I will not be able to continue the streak
# but i shall continue when I return home and am able to do so.
# its been a great run, one shot the entire time. definitely proud 

def solution(string):
    return string[::-1]