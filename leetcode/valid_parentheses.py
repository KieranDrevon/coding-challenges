# Given a string s containing just the characters 
# '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

# An input string is valid if:

# Open brackets must be closed by the same type of brackets.
# Open brackets must be closed in the correct order.
# Every close bracket has a corresponding open bracket of the same type.
 

def valid_parentheses(s):
    stack = []
    
    close_map = {
            "(": ")",
            "{": "}",
            "[": "]",
        }
        
    for br in s:
        if br in close_map:
            stack.append(br)
        elif len(stack) == 0 or br != close_map[stack.pop()]:
            return False
    return len(stack) == 0
