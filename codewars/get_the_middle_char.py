# You are going to be given a word. Your job is to return the middle character of the word. If the word's length is odd, return the middle character. If the word's length is even, return the middle 2 characters.

def get_middle(s):
    leng = len(s)
    
    if leng % 2 == 0:
        return s[leng // 2 - 1:leng // 2 + 1]
    else:
        return s[leng // 2]