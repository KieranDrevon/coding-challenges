# Given an array of integers.

# Return an array, where the first element is the count of positives numbers and the second element is sum of negative numbers. 0 is neither positive nor negative.

# If the input is an empty array or is null, return an empty array.

# Example
# For input [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15], you should return [10, -65].

# long workday, just wanted to get a problem done. Being suggested 5 kyu and had to skip as my brain is fried for the day. 

def count_positives_sum_negatives(arr):
    positive = 0
    
    negative = 0
    
    if len(arr) == 0:
        return []
    
    for num in arr:
        if num > 0:
            positive += 1
        elif num < 0:
            negative += num
            
    return [positive, negative]