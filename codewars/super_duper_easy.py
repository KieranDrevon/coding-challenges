# Make a function that returns the value multiplied by 50 and increased by 6. If the value entered is a string it should return "Error".

# Easy one today, going to hop into some java

def problem(a):
    #Easy Points ^_^
    if isinstance(a, str):
        return "Error"
    
    return (a * 50) + 6