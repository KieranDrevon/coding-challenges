# Covered insertion sort today, apparently this sort is too slow for the correct solution to the leetcode problem
# Will need to use merge sort for the correct solution, which I will cover in the next few days

def sortArray(nums):

    for i in range(1, len(nums)):
        j = i - 1
        while j >= 0 and nums[j + 1] < nums[j]:
            tmp = nums[j + 1]
            nums[j + 1] = nums[j]
            nums[j] = tmp
            j -= 1
    return nums

print(sortArray([5,2,3,1]))