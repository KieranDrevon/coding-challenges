# Given a list of digits, return the smallest number that could be formed from these digits, using the digits only once (ignore duplicates).

def min_value(digits):
    res = []
    digits.sort()
    
    for num in digits:
        if str(num) not in res:
            res.append(str(num))
            
    string = ''.join(res)
            
    return int(string)
        