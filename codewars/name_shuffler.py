# Write a function that returns a string in which firstname is swapped with last name.

# Example(Input --> Output)

# "john McClane" --> "McClane john"

def name_shuffler(str_):
    name_list = str_.split()
    
    return f"{name_list[1]} {name_list[0]}"