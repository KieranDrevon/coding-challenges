# Given an array of integers as strings and numbers, return the sum of the array values as if all were numbers.

# Return your answer as a number.


def sum_mix(arr):
    #your code here
    sum = 0
    
    for item in arr:
        if isinstance(item, int):
            sum += item
        elif isinstance(item, str):
            sum += int(item)
    return sum