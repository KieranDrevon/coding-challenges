# The main idea is to count all the occurring characters in a string. 
# If you have a string like aba, then the result should be {'a': 2, 'b': 1}.

# What if the string is empty? 
# Then the result should be empty object literal, {}.

def count(string):
    # The function code should be here
    if len(string) == 0:
        return {}
    
    values = {}
    
    for char in string:
        if char not in values:
            values[char] = 1
        else:
            values[char] += 1
        
    return values